#!/usr/bin/env python3

import sys
import pathlib
import glob

def scrub(_file_name):
    path = pathlib.Path(_file_name).resolve()

    with open(path, "r") as fs:
        contents = fs.read()

    scrubbed_content = contents.replace(u'\ufeff', '').replace('\u200b', '')

    with open(path, "w") as fs:
        contents = fs.write(scrubbed_content)

    print(f"{path} scrubbed.")

## CHECK right amount of args. 
if len(sys.argv) == 1:
    print("Incorrect number of args. To use scrubber run \"scrubw <file_path>\"", file = sys.stderr)
    exit(1) #exit with error
elif len(sys.argv) == 2:


    file_name = sys.argv[1]
    path = pathlib.Path(file_name).resolve()

    if path.is_file():
        scrub(file_name)
        exit(0)

    if path.is_dir():
        #Find all files recursively and scrub them. 
        files = [p for p in path.glob("*") if p.is_file()]

        value = str(input(f"Recursively scrub {len(files)} files?[Y]/n:\n") or "y").lower()
        if value == "n":
            exit(0)
        elif value == "y":
            paths = [*path.glob("*")]
            for p in paths:
                if p.is_file() == True:
                    scrub(p)
            exit(0)
else:
    file_names = sys.argv[1:]    
    paths = [pathlib.Path(name).resolve() for name in file_names if pathlib.Path(name).resolve().is_file()]
    value = str(input(f"Scrub {len(paths)} files?[Y]/n:\n") or "y").lower()

    if value == "n":
        exit(0)
    elif value == "y":
        for p in paths:
            if p.is_file() == True:
                scrub(p)