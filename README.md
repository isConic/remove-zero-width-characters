# Remove Zero Width Characters
Zero width characters are annoying. They show up time to time in the code bases I work with. They're annoying to deal with as they break my linters.
These are scripts and tools to remove them. 

# Usage:
CLI piping:
```
cat test.txt | python3 pipeable_scrub.py > scrubbed_test.txt
```

CLI direct:
```
python3 scrub.py test.txt
```

Add as command
```bash
chmod +x scrub.py   #marks as executable
#--> on your own add #!/usr/bin/env python3 to scrub.py to set the interpreter (if planning to execute on its own). There should already be something in scrub.py
cp scrub.py scrub_w #drops python extension and makes it look more like a stand alone command. 
mkdir -p ~/my_bin
cp scrub_w ~/my_bin
export PATH=$PATH":$HOME/my_bin"
```
At this point your `/my_bin` directory is only temporarily referenced. Edit your bash profile and add 
`export PATH=$PATH":$HOME/my_bin"` to it. 
